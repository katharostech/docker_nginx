##################
# Alpine Nginx
##################

FROM alpine

MAINTAINER Katharos Technology

# Install Nginx
RUN apk add --no-cache nginx

# Create directories not created by the install
RUN mkdir /run/nginx/
RUN mkdir -p /usr/share/nginx/html

# Copy our configuration files
COPY my-default.conf /etc/nginx/conf.d/my-default.conf
RUN chmod 644 /etc/nginx/conf.d/my-default.conf

COPY nginx.conf /etc/nginx/nginx.conf
RUN chmod 644 /etc/nginx/nginx.conf

# Nginx hosts on 80
EXPOSE 80

CMD ["nginx"]
